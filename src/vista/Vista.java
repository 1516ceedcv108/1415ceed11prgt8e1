
package vista;

import controlador.Controlador;
import modelo.Alumno;

/**
 * Ejercicio 1415ceed11prgt8e1
 *
 * @author Paco Angulo Grancha <francisco.angulog@gmail.com>
 * @date 29/03/2015
 */

public interface Vista {

    static final String CREATE = "Create";
    static final String READ = "Read";
    static final String UPDATE = "Update";
    static final String DELETE = "Delete";
    static final String EXIT = "Exit";

    // Define los eventos CRUD
    void setControlador(Controlador c);

    // comienza la visualización
    void arranca();

    // Funcion que crea el alumno del formulario en el modelo
    public String pedirId();

    // Se sale de la aplicación
    public void exit();

    // Muestra mensaje de error
    public void error(String error);

    // Muestra los datos de un alumno
    public void mostrarAlumno(Alumno alumno);

    // Pide los datos del alumno
    public Alumno pedirAlumno();

}
