
package controlador;

import java.io.IOException;
import modelo.Modelo;
import modelo.ModeloMysql;
import vista.Vista;
import vista.VistaPantalla;

/**
 * Ejercicio 1415ceed11prgt8e1
 *
 * @author Paco Angulo Grancha <francisco.angulog@gmail.com>
 * @date 29/03/2015
 */

public class Main {


    public static void main(String[] args) throws IOException {

        //Modelo modelo = new ModeloVector();
        //Modelo modelo = new ModeloArrayList();
        //Modelo modelo = new ModeloHashSet();
        //Modelo modelo = new ModeloFichero();
        Modelo modelo = new ModeloMysql();

        //Vista vista = new VistaTerminal();
        Vista vista = new VistaPantalla();

        Controlador c = new Controlador(modelo, vista);

    }

}
