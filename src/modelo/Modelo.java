
package modelo;

/**
 * Ejercicio 1415ceed11prgt8e1
 *
 * @author Paco Angulo Grancha <francisco.angulog@gmail.com>
 * @date 29/03/2015
 */

public interface Modelo {

    public void create(Alumno alumno); // Crea un alumno nuevo

    public void update(Alumno alumno); // Actuzaliza el alumno.

    public void delete(Alumno alumno);  // Borrar el alunno con el id dado

    public Alumno read(String id);  // Obtiene un alumno

}
