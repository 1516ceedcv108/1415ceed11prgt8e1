
package modelo;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Ejercicio 1415ceed11prgt8e1
 *
 * @author Paco Angulo Grancha <francisco.angulog@gmail.com>
 * @date 29/03/2015
 */

public class ModeloHashSet implements Modelo {

    HashSet alumnos = new HashSet();
    int id = 0;

    @Override
    public void create(Alumno alumno) {
        alumno.setId(id + "");
        alumnos.add(alumno);
        id++;
    }

    @Override
    public Alumno read(String id) {

        Alumno alumno = null;

        Iterator it = alumnos.iterator();
        while (it.hasNext()) {
            alumno = (Alumno) it.next();
            if (alumno.getId().equals(id)) {
                break;
            }

        }
        return alumno;
    }

    public void update(Alumno alumno) {

        Iterator it = alumnos.iterator();
        Alumno a;
        int pos = 0;

        while (it.hasNext()) {
            a = (Alumno) it.next();
            if (a.getId().equals(alumno.getId())) {
                alumnos.remove(a);
                alumnos.add(alumno);
            }
        }
    }

    public void delete(Alumno alumno) {

        Iterator it = alumnos.iterator();
        Alumno a;
        int pos = 0;

        while (it.hasNext()) {
            a = (Alumno) it.next();
            if (a.getId().equals(alumno.getId())) {
                alumnos.remove(a);

            }
        }

    }

}
