
package modelo;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Ejercicio 1415ceed11prgt8e1
 *
 * @author Paco Angulo Grancha <francisco.angulog@gmail.com>
 * @date 29/03/2015
 */

public class ModeloArrayList implements Modelo {

    ArrayList<Alumno> alumnos = new ArrayList<Alumno>();
    int id = 0;

    @Override
    public void create(Alumno alumno) {
        alumno.setId(id + "");
        alumnos.add(alumno);
        id++;
    }

    public Alumno read(String id) {

        Alumno alumno = null;

        Iterator it = alumnos.iterator();
        while (it.hasNext()) {
            alumno = (Alumno) it.next();
            if (id.equals(alumno.getId())) {
                break;
            }

        }
        return alumno;
    }

    public void update(Alumno alumno) {

        Alumno a;
        int pos = 0;

        Iterator it = alumnos.iterator();
        while (it.hasNext()) {
            a = (Alumno) it.next();
            if (a.getId().equals(alumno.getId())) {
                alumnos.set(pos, alumno);
            }
        }

    }

    @Override
    public void delete(Alumno alumno) {

        Iterator it = alumnos.iterator();
        Alumno a;
        int pos = 0;

        while (it.hasNext()) {
            a = (Alumno) it.next();
            if (a.getId().equals(alumno.getId())) {
                alumnos.remove(a);
            }
            pos++;
        }
    }

}
