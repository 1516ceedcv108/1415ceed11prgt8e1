package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Ejercicio 1415ceed11prgt8e1
 *
 * @author Paco Angulo Grancha <francisco.angulog@gmail.com>
 * @date 29/03/2015
 */
public class ModeloMysql implements Modelo {

    String jdbcUrl = "jdbc:mysql://127.0.0.1:3306/ceedprgt8";
    Connection con = null;

    private static class driver {

        public driver() throws ClassNotFoundException, InstantiationException, IllegalAccessException {

            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
        }
    }

    @Override
    public void create(Alumno alumno) {

        //Connection con = null;
        
        try {
            con = DriverManager.getConnection(jdbcUrl, "alumno", "alumno");
            Statement st = con.createStatement();
                        
            String sql = "insert into alumnos (nombre,edad,email) values ('"+alumno.getNombre()+"',"+alumno.getEdad()+",'"+alumno.getEmail()+"');";

            st.executeUpdate(sql);
            con.close();

        } catch (SQLException ex) {
            System.out.println("Error tip: " + ex);
        }
    }

    @Override
    public void update(Alumno alumno) {

        //Connection con = null;

        try {
            con = DriverManager.getConnection(jdbcUrl, "alumno", "alumno");
            Statement st = con.createStatement();
            
            String sql = "update alumnos set nombre='"+alumno.getNombre()+"', edad='"+alumno.getEdad()+"', email= '"+alumno.getEmail()+"' where id="+alumno.getId();
            
            st.executeUpdate(sql);
            con.close();

        } catch (SQLException ex) {
            System.out.println("Error tip: " + ex);
        }
    }

    @Override
    public void delete(Alumno alumno) {

        //Connection con = null;

        try {
            con = DriverManager.getConnection(jdbcUrl, "alumno", "alumno");
            Statement st = con.createStatement();
            String sql = "delete from alumnos where id=" + alumno.getId();
            st.executeUpdate(sql);
            con.close();

        } catch (SQLException ex) {
            System.out.println("Error tip: " + ex);
        }
    }

    @Override
    public Alumno read(String id) {

        //Connection con = null;
        Alumno alumno = null;

        try {
            con = DriverManager.getConnection(jdbcUrl, "alumno", "alumno");
            Statement st = con.createStatement();
            String sql = "select nombre, edad, email from alumnos where id=" + id;
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String nombre = rs.getString("nombre");
                int edad = rs.getInt("edad");
                String email = rs.getString("email");

                alumno = new Alumno(id, nombre, edad, email);                
            }
            con.close();

        } catch (SQLException ex) {
        }
        return alumno;
    }
}